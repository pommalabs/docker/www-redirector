#!/bin/sh
set -e

envsubst '$WWW_REDIRECTOR_HOST' \
  < $WWW_REDIRECTOR_PATH/templates/$WWW_REDIRECTOR_MODE.conf.template \
  > $WWW_REDIRECTOR_PATH/conf.d/www-redirector.conf

nginx -g "daemon off;"
