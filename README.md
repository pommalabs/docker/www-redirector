# WWW Redirector Docker image

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]

Redirects all non-www traffic to www (or vice versa).

## Table of Contents

- [WWW Redirector Docker image](#www-redirector-docker-image)
  - [Table of Contents](#table-of-contents)
  - [Tags](#tags)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
    - [Building Docker image](#building-docker-image)
  - [License](#license)

## Tags

Following tags are available:

| Tag      | Base image       |
|----------|------------------|
| `latest` | `nginx:1-alpine` |

Each tag can be downloaded with following command, just replace `latest` with desired tag:

```bash
docker pull container-registry.pommalabs.xyz/pommalabs/www-redirector:latest
```

Tags are rebuilt every week by a job scheduled on GitLab CI platform.

## Usage

Image behavior can be configured using following environment variables:

| Environment variable  | Description                                                                    |
|-----------------------|--------------------------------------------------------------------------------|
| `WWW_REDIRECTOR_MODE` | `non-www-to-www`, `www-to-non-www`, `fixed-host`.                              |
| `WWW_REDIRECTOR_HOST` | When mode is set to `fixed-host`, all traffic is redirected to specified host. |

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.
I replaced the __Install__ section with __Tags__, since I thought that it made no sense
to "install" an helper Docker image.

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag name (e.g. `www-redirector`).

## License

MIT © 2020-2024 [PommaLabs Team and Contributors][pommalabs-website]

[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/docker/www-redirector/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/docker/www-redirector/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/docker/www-redirector/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
